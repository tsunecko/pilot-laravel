<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role === array_search(User::TYPE_ADMIN, User::TYPES_ROLES)) {
            return $next($request);
        }

        return redirect()->back()->with('successMsg', 'You are not allowed.');
    }
}
