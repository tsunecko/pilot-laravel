<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    const TYPE_NEWS = 'news';
    const TYPE_AVIA = 'avia games';
    const TYPE_COSMO = 'cosmo games';

    const TYPES = [
        self::TYPE_NEWS,
        self::TYPE_AVIA,
        self::TYPE_COSMO,
    ];

    protected $fillable = [
        'title', 'slug', 'text', 'type', 'author_id', 'paid', 'deleted_at'
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updates_at'
    ];

    protected $casts = [
        'deleted_at' => 'datetime',
        'paid' => 'boolean',
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
