<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstalledGame extends Model
{
    const TYPE_DCS = 'DCS';
    const TYPE_ED = 'Elite Dangerous';
    const TYPE_FC = 'Flying Circus';
    const TYPE_IL2_CLIFFS = 'IL-2 Cliffs of Dover Blitz';
    const TYPE_IL2 = 'IL-2 Sturmovik Great Battles';
    const TYPE_IL2_OLD = 'IL-2 Sturmovik (old)';
    const TYPE_MSFS = 'MSFS';
    const TYPE_SC = 'Star Citizen';
    const TYPE_WT = 'War Thunder';
    const TYPE_X_Plane = 'X-Plane';

    const TYPES = [
        self::TYPE_DCS,
        self::TYPE_ED,
        self::TYPE_FC,
        self::TYPE_IL2_CLIFFS,
        self::TYPE_IL2,
        self::TYPE_IL2_OLD,
        self::TYPE_MSFS,
        self::TYPE_SC,
        self::TYPE_WT,
        self::TYPE_X_Plane,
    ];

    protected $fillable = [
        'type', 'url', 'user_id',
    ];

    protected $dates = [
        'created_at', 'updates_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
