<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    const TYPE_SITE = 'site';
    const TYPE_FB = 'facebook';
    const TYPE_INSTA = 'instagram';
    const TYPE_TWICH = 'twich';
    const TYPE_YOUTUBE = 'youtube';
    const TYPE_VK = 'vk';

    const TYPES = [
        self::TYPE_SITE,
        self::TYPE_FB,
        self::TYPE_INSTA,
        self::TYPE_TWICH,
        self::TYPE_YOUTUBE,
        self::TYPE_VK,
    ];

    protected $fillable = [
       'type', 'url', 'user_id',
    ];

    protected $dates = [
        'created_at', 'updates_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
