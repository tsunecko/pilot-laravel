<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const TYPE_USER = 'user';
    const TYPE_ADMIN = 'admin';
    const TYPE_SELLER = 'seller';
    const TYPE_PRIVATE_CRAFTER = 'private crafter';
    const TYPE_MANUFACTURE = 'manufacture';

    const TYPES_ROLES = [
        self::TYPE_USER,
        self::TYPE_ADMIN,
        self::TYPE_SELLER,
        self::TYPE_PRIVATE_CRAFTER,
        self::TYPE_MANUFACTURE,
    ];

    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'tel', 'role', 'avatar', 'first_name', 'last_name', 'country', 'city',
        'note', 'installed_game_id', 'email_verified_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'created_at', 'updates_at'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'fullname',
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function sites()
    {
        return $this->hasMany(Site::class);
    }

    public function installedGames()
    {
        return $this->hasMany(InstalledGame::class);
    }

    public function getFullnameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

}
