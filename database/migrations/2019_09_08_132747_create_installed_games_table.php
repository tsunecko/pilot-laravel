<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstalledGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installed_games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('url');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('installed_games', function (Blueprint $table) {
            $table->dropForeign('installed_games_user_id_foreign');
        });

        Schema::dropIfExists('installed_games');
    }
}
