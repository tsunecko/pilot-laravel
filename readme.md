# Pilot

Application provides store with aviadetails, news about connected themes with avia and communication inside system

## Run
Requirements:
```
apache2
mysql server
PHP >= 7.2.0
node
npm
composer
```

Run migration and seed:
```
php artisan migrate --seed
```
Build Laravel mix (minify):
```
npm run production
```

Run Laravel server:
```
php artisan serve
```


