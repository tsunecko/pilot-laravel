<?php

return [
    'accepted' => 'Поле :attribute должно быть принято.',
    'active_url' => 'Поле :attribute не является подходящим e-mail.',
    'after' => 'Поле :attribute должно быть датой после :date.',
    'after_or_equal' => 'Поле :attribute должно быть равно или позже :date.',
    'alpha' => 'Поле :attribute должно состоять только из букв.',
    'alpha_dash' => 'Поле :attribute должно состоять только из букв, цифр, тире и нижнего подчеркивания.',
    'alpha_num' => 'Поле :attribute должно состоять только из букв и цифр.',
    'array' => 'Поле :attribute должно быть массивом.',
    'before' => 'Поел :attribute должно быть датой перед :date.',
    'before_or_equal' => 'Поле :attribute должно быть равным или позже to :date.',
    'between' => [
        'numeric' => 'Поле :attribute должно быть между :min и :max.',
        'file' => 'Поле :attribute должно быть между :min и :max килобайтами.',
        'string' => 'Поле :attribute должно быть между :min и :max символами.',
        'array' => 'Поле :attribute должно быть между :min и :max предметами.',
    ],
    'boolean' => 'Поле :attribute должно быть истиной или ложью.',
    'confirmed' => 'Поле :attribute не созпадает с подтверждаемым полем.',
    'date' => 'Поле :attribute не явзяется подходящей датой.',
    'date_equals' => 'Поле :attribute должно быть датой равной :date.',
    'date_format' => 'Поле :attribute не совпадает с форматом :format.',
    'different' => 'Поле :attribute и :other должны быть разными.',
    'digits' => 'Поле :attribute должно быть длиной :digits.',
    'digits_between' => 'Поле :attribute должно быть между длинами :min и :max.',
    'dimensions' => 'Поле :attribute имеет неправильное разрешение изображения.',
    'distinct' => 'Поле :attribute имеет повторяющееся значение.',
    'email' => 'Поле :attribute должно быть подходящим email адресом.',
    'ends_with' => 'Поле :attribute должно быть одним из: :values',
    'exists' => 'Это поле :attribute недействительно.',
    'file' => 'Поле :attribute должно быть файлом.',
    'filled' => 'Поле :attribute должно иметь значение.',
    'gt' => [
        'numeric' => 'Поле :attribute должно быть быльше чем :value.',
        'file' => 'Поле :attribute должно быть больше чем :value килобайт.',
        'string' => 'Поле :attribute должно быть больше чем :value символов.',
        'array' => 'Поле :attribute должно быть больше чем :value предметов.',
    ],
    'gte' => [
        'numeric' => 'Поле :attribute должно быть больше или равно :value.',
        'file' => 'Поле :attribute должно быть больше или равно :value килобайт.',
        'string' => 'Поле :attribute должно быть больше или равно :value символов.',
        'array' => 'Поле :attribute должно иметь :value предметов или больше.',
    ],
    'image' => 'Поле :attribute должно быть изображением.',
    'in' => 'Выбранный :attribute недействительный.',
    'in_array' => 'Поле :attribute не существует в :other.',
    'integer' => 'Поле :attribute должно быть цифрой.',
    'ip' => 'Поле :attribute должно быть действительным IP адресом.',
    'ipv4' => 'Поле :attribute должно быть действительным IPv4 адресом.',
    'ipv6' => 'Поле :attribute должно быть действительным IPv6 адресом.',
    'json' => 'Поле :attribute должно быть действительной JSON строкой.',
    'lt' => [
        'numeric' => 'Поле :attribute должно быть меньше чем :value.',
        'file' => 'Поле :attribute должно быть меньше чем :value килобайт.',
        'string' => 'Поле :attribute должно быть меньше чем :value символов.',
        'array' => 'Поле :attribute должно быть меньше чем :value предметов.',
    ],
    'lte' => [
        'numeric' => 'Поле :attribute должно быть меньше или равно :value.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'file' => 'Поле :attribute должно быть меньше или равно :value килобайт.',
        'array' => 'Поле :attribute должно быть меньше или равно :value предметов.',
    ],
    'max' => [
        'numeric' => 'Поле :attribute должно быть больше чем :max.',
        'file' => 'Поле :attribute должно быть не больше чем :max килобайт.',
        'string' => 'Поле :attribute должно быть не больше чем :max символов.',
        'array' => 'Поле :attribute должно быть не больше :max предметов.',
    ],
    'mimes' => 'Поле :attribute должно быть файлом типа: :values.',
    'mimetypes' => 'Поле :attribute должно быть файлом типов: :values.',
    'min' => [
        'numeric' => 'Поле :attribute должно быть минимум :min.',
        'file' => 'Поле :attribute должно быть минимум :min килобайт.',
        'string' => 'Поле :attribute должно быть минимум :min символов.',
        'array' => 'Поле :attribute должно быть минимум :min предметов.',
    ],
    'not_in' => 'Выбранный :attribute недействительный.',
    'not_regex' => 'Формат поля :attribute недействительный.',
    'numeric' => 'Поле :attribute должно быть цифрой.',
    'present' => 'Поле :attribute должно быть из присутствующих полей.',
    'regex' => 'Формат поля :attribute недействительный.',
    'required' => 'Поле :attribute обязательное.',
    'required_if' => 'Поле :attribute обязательное, если :other имеет значение :value.',
    'required_unless' => 'Поле :attribute обязательной пока :other в :values.',
    'required_with' => 'Поле :attribute обязательно когда :values присутствует.',
    'required_with_all' => 'Поле :attribute обязательно когда :values присутствуют.',
    'required_without' => 'Поле :attribute обязательно когда :values не присутствует.',
    'required_without_all' => 'Поле :attribute обязательно когда ни одно из :values не присутствует.',
    'same' => 'Поле :attribute и :other должны совпадать.',
    'size' => [
        'numeric' => 'Поле :attribute должно быть размером :size.',
        'file' => 'Поле :attribute должно быть размером :size килобайт.',
        'string' => 'Поле :attribute должно быть размером :size сивмолов.',
        'array' => 'Поле :attribute должно содержать :size предметов.',
    ],
    'starts_with' => 'Поле :attribute должно начинаться с: :values',
    'string' => 'Поле :attribute должно быть строкой.',
    'timezone' => 'Поле :attribute должен быть подходящим веремнным поясом.',
    'unique' => 'Поле :attribute уже занятно.',
    'uploaded' => 'Произошла ошибка при загрузке поля :attribute.',
    'url' => 'Формат поля :attribute недействительный.',
    'uuid' => 'Поле :attribute должно быть действительным UUID.',

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'кастомное-сообщение',
        ],
    ],

    'attributes' => [],
];
