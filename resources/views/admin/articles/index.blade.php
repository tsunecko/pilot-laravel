@extends('layouts.admin')

@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    <div class="col-12">
    <div class="my-3 p-3 bg-white rounded shadow-sm">

        <h6 class="border-bottom border-gray pb-2 mb-0">
            Articles
        </h6>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead><tr>
                    <th class="text-muted sort">#</th>
                    <th class="text-muted sort">Title</th>
                    <th class="text-muted sort">Type</th>
                    <th class="text-muted sort">Author</th>
                    <th class="text-muted sort">Paid</th>
                    <th class="text-muted sort">Created</th>
                    <th class="text-muted sort">Updated</th>
                </tr></thead>

                <tbody>
                @forelse($articles as $article)
                    <tr>
                        <td>{{ $article->id }}</td>
                        <td>{{ $article->title }}</td>
                        <td>{{ App\Models\Article::TYPES[$article->type] }}</td>
                        <td>{{ $article->author->name }}</td>
                        <td>{{ $article->paid }}</td>
                        <td>{{ $article->created_at }}</td>
                        <td>{{ $article->updated_at }}</td>
                    </tr>
                @empty
                    <tr><td colspan="7">No articles</td></tr>
                @endforelse
                </tbody>
            </table>
        </div>

    </div>
    </div>

@endsection

