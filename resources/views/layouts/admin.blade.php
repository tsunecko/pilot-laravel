<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/admin.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>

<body>
<div id="app">
    <!-- Vertical navbar -->
    <nav class="vertical-nav bg-white" id="sidebar">

        <!---- User info ---->
        <div class="py-4 px-3 mb-4 bg-light">
            <div class="media d-flex align-items-center">
                <img src="{{ Auth::user()->avatar ?? asset('images/default.svg') }}"
                     alt="avatar" width="65" class="rounded-circle img-thumbnail shadow-sm">
                <div class="media-body">
                    <h4 class="m-0">{{ Auth::user()->name }}</h4>
                    <p class="font-weight-light text-muted mb-0">{{ App\Models\User::TYPE_ADMIN }}</p>
                </div>
            </div>
        </div>
        <!---- /User info ---->

        <!---- Language ---->
        <div class="px-3 pb-3 dropdown-lang">
            <button class="dropbtn-lang">{{ __('Select language') }} <i class="fas fa-sort-down"></i></button>
            <div class="dropdown-lang-content">
                <a href="{{ route('lang', ['locale' => 'en']) }}" id="en">
                    {{ __('English') }}
                </a>
                <a href="{{ route('lang', ['locale' => 'ru']) }}" id="ru">
                    {{ __('Russian') }}
                </a>
                <a href="{{ route('lang', ['locale' => 'ua']) }}" id="ua">
                    {{ __('Ukrainian') }}
                </a>
            </div>
        </div>
        <!---- Language ---->

        <p class="text-gray font-weight-bold text-uppercase px-3 small pb-4 mb-0">
            {{ __('Main') }}
        </p>

        <ul class="nav flex-column bg-white mb-0">
            <!---- Home ---->
            <li class="nav-item">
                <a href="{{ route('admin.index') }}" class="nav-link text-dark font-italic bg-light">
                    <i class="mr-3 fas fa-home text-primary fa-fw"></i>
                    <span>{{ __('Home') }}</span>
                </a>
            </li>
            <!---- Articles ---->
            <li class="nav-item">
                <a href="{{ route('admin.articles.index') }}" class="nav-link text-dark font-italic bg-light">
                    <i class="mr-3 far fa-newspaper text-primary fa-fw"></i>
                    <span>{{ __('Articles') }}</span>
                </a>
            </li>
        </ul>

        <p class="text-gray font-weight-bold text-uppercase px-3 small py-4 mb-0">
            {{ __('Charts') }}
        </p>

        <ul class="nav flex-column bg-white mb-0">
            <!---- Charts ---->
            <li class="nav-item">
                <a href="#" class="nav-link text-dark font-italic">
                    <i class="mr-3 fas fa-chart-bar text-primary fa-fw"></i>
                    <span>{{ __('Area charts') }}</span>
                </a>
            </li>
        </ul>
    </nav>
    <!---- End vertical navbar ---->

    <!---- Main content ---->
    <main class="py-4">
        <div class="page-content p-5" id="content">
            <div class="container">
                <div class="row">
                    <button id="sidebarCollapse" type="button"
                            class="btn btn-light bg-white rounded-pill shadow-sm px-4 mb-4">
                        <i class="fa fa-bars mr-2"></i>
                        <small class="text-uppercase font-weight-bold">{{ __('Toggle') }}</small>
                    </button>
                </div>

                <div class="row">
                    @yield('content')
                </div>

            </div>
        </div>
    </main>
    <!---- /Main content ---->

</div>
</body>
</html>
