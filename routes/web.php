<?php

Auth::routes();

Route::get('lang/{locale}', 'v1\LocalizationController@index')
    ->name('lang');

// Controllers Within The "App\Http\Controllers\Admin" Namespace
// Matches The "/admin/..." URL
// Only auth, allow access 60 times per minute
Route::namespace('v1\Admin')
    ->prefix('admin')
    ->middleware(['auth', 'auth.admin', 'throttle:60,1'])
    ->group(function () {

        Route::get('/', 'HomeController@index')->name('admin.index');
        Route::get('/articles', 'ArticleController@index')->name('admin.articles.index');

});

Route::middleware('auth')
    ->get('/', 'v1\HomeController@index')
    ->name('home');
